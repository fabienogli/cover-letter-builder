package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/fabienogli/cover-letter-builder/generator"
)

func main() {
	companyPtr := flag.String("comp", "", "Company name")
	posPtr := flag.String("pos", "Software Engineer Intern", "Position name")
	langPtr := flag.String("lang", "en", "Language of Cover Letter")
	refPtr := flag.Bool("ref", false, "Letter of reference")
	flag.Parse()

	if *companyPtr == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	var skills []string
	if len(flag.Args()) < 1 {
		skills = []string{"golang", "java", "bash"}
	} else {
		skills = flag.Args()
	}
	write(*companyPtr, *posPtr, *langPtr, *refPtr, skills)
}

func write(company, job, lang string, ref bool, skills []string) {

	location := company + ".doc"
	f, err := os.Create(location)
	check(err)
	coverLetter := generator.Generate(company, job, lang, ref, skills)
	_, err = f.WriteString(coverLetter)
	check(err)
	_ = f.Sync()
	fmt.Printf("Cover Letter Saved in: %s", location)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
