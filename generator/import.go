package generator

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

const fileName = "seed.json"

func read(fileName string) (CoverLetter, error) {
	byteValue, err := GetFile()
	if err != nil {
		return CoverLetter{}, err
	}
	var coverLetter CoverLetter
	err = json.Unmarshal(byteValue, &coverLetter)
	if err != nil {
		return CoverLetter{}, err
	}
	return coverLetter, nil
}

func getProjectDirectory() (string, error) {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return "", fmt.Errorf("project directory not found..")
	}
	parent := path.Dir(filename)
	return path.Dir(parent), nil
}

func GetFile() ([]byte, error) {
	directory, err := getProjectDirectory()
	if err != nil {
		return nil, err
	}
	absPath := directory + "/" + fileName
	fmt.Printf("importing from %s\n", absPath)

	jsonFile, err := os.Open(absPath)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	return byteValue, err
}

func GetCoverLetter(skills []string) (CoverLetter, error) {
	filePath, err := filepath.Abs(fileName)
	if err != nil {
		panic(err)
	}
	coverLetter, err := read(filePath)
	if err != nil {
		return CoverLetter{}, err
	}
	coverLetter.onlyKeep(skills)
	return coverLetter, err

}

func (coverLetter *CoverLetter) onlyKeep(names []string) {
	var skills []Skill
	for _, skill := range coverLetter.Skills {
		for _, name := range names {
			if name == skill.Name {
				skills = append(skills, skill)
			}
			continue
		}
	}
	coverLetter.Skills = skills
}
