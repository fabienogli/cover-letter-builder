package generator

import (
	"log"
	"strings"
)

const (
	COMPANY  = "COMPANY_NAME"
	POSITION = "POSITION_NAME"
	FR       = "fr"
	EN       = "en"
)

type Description struct {
	En string `json:"en"`
	Fr string `json:"fr"`
}

type Information struct {
	Name        string      `json:"name"`
	Description Description `json:"description"`
}

type RatedInformation struct {
	Name        string      `json:"name"`
	Description Description `json:"description"`
	Rate        int         `json:"rate"`
}

type Skill RatedInformation
type Experience RatedInformation

type CoverLetter struct {
	Informations []Information `json:"informations"`
	Experiences  []Experience  `json:"experiences"`
	Skills       []Skill       `json:"skills"`
}

func Generate(company, position, lang string, ref bool, skills []string) string {
	draft := generateRaw(skills, lang)
	coverLetter := process(company, position, draft, ref)
	return coverLetter
}

func generateRaw(_skills []string, lang string) string {
	cover, err := GetCoverLetter(_skills)
	if err != nil {
		log.Println(err)
	}
	s, err := cover.ToRaw(lang)
	if err != nil {
		log.Println(err)
	}
	return s
}

func process(companyName, jobName, draft string, ref bool) string {
	arrDraft := strings.Split(draft, "[")
	coverLetter := arrDraft[0]
	for i := 1; i < len(arrDraft); i++ {
		tmp := strings.Split(arrDraft[i], "]")
		if len(tmp) > 1 {
			if tmp[0] == COMPANY {
				coverLetter += companyName
			} else if tmp[0] == POSITION {
				coverLetter += jobName
			} else if ref { //this is ugly but works for now
				coverLetter += tmp[0]
			}
			coverLetter += tmp[1]
		} else {
			coverLetter += tmp[0]
		}
	}
	return coverLetter
}

func (cover CoverLetter) GetJob() (Information, error) {
	return cover.getInformation("job_description")
}

func (cover CoverLetter) GetPresentation() (Information, error) {
	return cover.getInformation("presentation")
}

func (cover CoverLetter) GetAvailability() (Information, error) {
	return cover.getInformation("availability")
}

func (cover CoverLetter) GetBye() (Information, error) {
	return cover.getInformation("bye")
}

func (cover CoverLetter) getInformation(name string) (Information, error) {
	for _, information := range cover.Informations {
		if information.Name == name {
			return information, nil
		}
	}
	return Information{}, nil
}

func (cover CoverLetter) GetExperience(name string) (Experience, error) {
	for _, experience := range cover.Experiences {
		if experience.Name == name {
			return experience, nil
		}
	}
	return Experience{}, nil
}

func (description Description) ToRaw(language string) string {
	if language == "fr" {
		return description.Fr
	} else {
		return description.En
	}
}

func (cover CoverLetter) ToRaw(lang string) (string, error) {
	job, _ := cover.GetJob()
	availability, _ := cover.GetAvailability()
	presentation, _ := cover.GetPresentation()

	optimy, _ := cover.GetExperience("optimy")
	var resume string
	resume += job.Description.ToRaw(lang) + "\n\n"
	resume += availability.Description.ToRaw(lang) + "\n"
	resume += presentation.Description.ToRaw(lang) + "\n"
	resume += optimy.Description.ToRaw(lang) + "\n"

	resume += "\n"
	for _, skill := range cover.Skills {
		resume += skill.Description.ToRaw(lang) + "\n"
	}
	bye, _ := cover.GetBye()
	resume += bye.Description.ToRaw(lang)
	return resume, nil
}

func (cover CoverLetter) String() string{
	 result, _ := cover.ToRaw("fr")
	 return result
 }