# cover-letter-builder

A builder of a cover letter, let you generate your cover letter with your experienced and everything

## Installation

`go install ./...`

## Requirements

You need a json file name `seed.json`, you can see copy paste the example and replace the informations with your own.  
If you want to quote the company, just write `[COMPANY_NAME]`  
If you want to quote the position, just write `[POSITION_NAME]`  
Hopefully you will find a job
